using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaGirlScript : MonoBehaviour
{
    public int i = 0;
    public GameObject ninjagirl;
    public GameObject[] zombie;
    private float distance;
    private int contadorZombie = 0;
    private int subeNivel = 2;
    private bool morir = false;
    private bool puedeSaltar = false;
    private Animator _animator;
    private Rigidbody2D rb2d;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(morir == false)
        { 
            rb2d.velocity = new Vector2(7+subeNivel,rb2d.velocity.y);
            if (Input.GetKeyDown(KeyCode.Space) && puedeSaltar==true)
            {
                float upSpeed = 25;
                setJumpAnimation();
                rb2d.velocity = Vector2.up * upSpeed;
                puedeSaltar = false;
                distance = Vector2.Distance(ninjagirl.transform.position, zombie[i].transform.position);
                SetDistance();
                if(distance <= 15 || distance >= 0)
                {
                    i++;
                    contadorZombie++;
                } 
                if(contadorZombie == 2)
                {
                    i = 0;
                    subeNivel += 5;
                }
            }

            if(puedeSaltar == true)
            {
                setRunAnimation();
            }
        }else
        {
            rb2d.velocity = new Vector2(0,rb2d.velocity.y);
            setDeadAnimation();
            UnityEngine.Debug.Break();
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(puedeSaltar == false)
        {
            puedeSaltar = true;
        }
        
        if(other.gameObject.tag == "Zombie")
        {
            morir = true;
        }
    }

    private void SetDistance()
    {
        Debug.Log("Distancia: " + distance.ToString());
    }    
    private void setRunAnimation(){
       _animator.SetInteger("State",0);
    }
    
    private void setJumpAnimation(){
        _animator.SetInteger("State",1);
    }

    private void setIdleAnimation(){
        _animator.SetInteger("State",2);
    }
    
    private void setDeadAnimation(){
        _animator.SetInteger("State",3);
    }

    private void setExit()
    {
        _animator.SetInteger("State",4);
    }

}