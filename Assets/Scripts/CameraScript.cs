using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public GameObject ninjagirl;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var x = ninjagirl.transform.position.x + 10;
        var y = ninjagirl.transform.position.y + 1;
        transform.position = new Vector3(x,y,transform.position.z);
    }
}
